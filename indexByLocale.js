const fs = require('fs')
//const dir = '/home/local/download_30-11_0-500_filtered/'
const dir = '/home/local/FINALFINALDIR/'
const files = fs.readdirSync(dir)
const filterKey = 'og:locale'
const rFile = 'rDataByLocale.R'
//const filterKey = 'tld'
//const rFile = 'rDataByTld.json'

/* array of sites which was already checked
 * i loop over all sites, because of the possibility 
 * that a miner was removed after some time */
const sites = [] 
const countries = [] // array with countries and number of miners
var counter = 0

// number of sites
console.log("Files: " + files.length)

readFiles().then(() => {
	
	readFiles().then(() => {
	        var keys = []	
		var values = []
		countries.map(function(v,i) {
	                keys.push('"' + v.key + '"')
	                values.push(v.value)
	        });

	        var RString = 'myVec=c('+values+'); names(myVec)=c('+keys+'); barplot(myVec, col=rgb(0.2,0.4,0.6,0.6), las=2)'

		 fs.writeFileSync(rFile, RString)
	});

	fs.writeFileSync(rFile, rString)
});

function readFiles() {
	
	return Promise.all(
		files.map(file => new Promise((resolve, reject) => {
			
			fs.readFile(dir + file, (err, data) => {
							
				console.log("read " + file)
				if (err) { return reject(err) }
				resolve(data)
			})
		
		}).then(data => {

			console.log("parse")

			const obj = JSON.parse(data) // parse json file 

			// check if there is a miner
			if(Object.keys(obj.miner).length) { 
				 // loop over meta array
				 obj.meta.map(item => {
					 if(item.property == filterKey && sites.toString().indexOf(obj.pageName) == -1) {
						 writeArray(item.content) // write data to array
						 sites.push(obj.pageName) // write pageName to array
					 }
				 })
				/*
				if(sites.toString().indexOf(obj.pageName) == -1) {
					writeArray(obj.tld)
					sites.push(obj.pageName)
				}*/
			}

		}).catch(error => {
			console.error(error)
			return false
		})
	))
}

function writeArray(code) {
	
	code = code.toLowerCase() 
	var check = countries.filter(item => item.key == code)

	// if code already in list, increment value
	if(check.length) {
		countries.filter(item => {      
			 return(item.key === code)
		}).map(function(item, i) {
			item.value = item.value+1
		})
		return false
	}
	
	// write data to array if doesnt exist
	countries.push({
		key:   code,
		value: 1
	}) 
}
